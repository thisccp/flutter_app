import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/ui/loginPage.dart';

Color fontColor = Color(0xFF2C333C);

void main (){
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoginPage(),
    theme: ThemeData(
      textTheme: TextTheme(
        body1: TextStyle(color: fontColor),
        button: TextStyle(color: fontColor),
        body2: TextStyle(color: fontColor),
        caption: TextStyle(color: fontColor),
        display1: TextStyle(color: fontColor),
        display2: TextStyle(color: fontColor),
        display3: TextStyle(color: fontColor),
        display4: TextStyle(color: fontColor),
        headline: TextStyle(color: fontColor),
        overline: TextStyle(color: fontColor),
        subhead: TextStyle(color: fontColor),
        subtitle: TextStyle(color: fontColor),
        title: TextStyle(color: fontColor),

      )
    ),
  ));
}