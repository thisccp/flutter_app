import 'package:flutter/material.dart';
import 'package:flutter_app/ui/classesPage.dart';
import 'package:auto_size_text/auto_size_text.dart';

Color mainColor = Color(0xFF8CE6C6);
Color fontColor = Color(0xFF2C333C);

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}

class EventPage extends StatefulWidget {
  @override
  _EventPageState createState() => _EventPageState();
}

// Variável que armazena o tamanho de alguma midia ou tela
MediaQueryData sizeScreen;

class _EventPageState extends State<EventPage> {
  @override
  Widget build(BuildContext context) {
    sizeScreen = MediaQuery.of(context); //armazena o contexto na varíavel sizeScreen
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Stack(
        //stack forma uma pilha e deixa o header atras do conteúdo do corpo no scroll
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: sizeScreen.size.height - 270.0),
            child: AppBar(
              elevation: 0.0,
              // header
              backgroundColor: Color(hexColor("#c7ffda")),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: 50.0,
              left: 20.0,
              right: 20.0
            ),
            child: Container(
              height: sizeScreen.size.height - 130.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.4), BlendMode.dstATop),
                    image: AssetImage('assets/background.jpg')),
                color: Color(0xFFF9F9F9),
                shape: BoxShape.rectangle,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey[300],
                      offset: Offset(0.0, 3.0),
                      blurRadius: 5.0)
                ],
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 50.0, left: 30.0),
                    child: Row(children: <Widget>[
                      AutoSizeText(
                        "Bem vindo",
                        maxLines: 1,
                        maxFontSize: 30.0,
                        minFontSize: 20.0,
                        style: TextStyle(
                          fontFamily: 'MontExtraLight',
                          fontSize: 30.0,
                        ),
                      ),
                    ]),
                  ),
                  Row(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 20.0, left: 80.0),
                      child: AutoSizeText(
                        "Prof.",
                        maxLines: 1,
                        maxFontSize: 25.0,
                        minFontSize: 15.0,
                        style: TextStyle(
                            fontFamily: 'MontExtraLight', fontSize: 25.0),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(top: 20.0, left: 10.0),
                        child: AutoSizeText(
                          "Thiago",
                          maxLines: 1,
                          maxFontSize: 38.0,
                          minFontSize: 28.0,
                          style: TextStyle(
                              fontFamily: 'MontHeavy', fontSize: 38.0),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 2.0, right: 2.0),
                        child: Container(
                            height: 100.0, width: 1.0, color: Colors.white))
                  ]),
                  Padding(
                    padding: EdgeInsets.only(top: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AutoSizeText(
                          "O que deseja enviar?",
                          maxLines: 1,
                          maxFontSize: 28.0,
                          minFontSize: 20.0,
                          style: TextStyle(
                              fontFamily: 'MontHeavy', fontSize: 28.0),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: sizeScreen.size.width - 230.00),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 38.0, right: 0.0),
                            child: Container(
                              width: (160.0),
                              height: 60.0,
                              child: RaisedButton(
                                elevation: 0.0,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      new MyCustomRoute(
                                          builder: (context) =>
                                              ClassesPage('ocorrencia')));
                                },
                                highlightColor: Colors.white70,
                                color: Color(hexColor("#c7ffda")),
                                shape: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 0.5),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(7.0),
                                    bottomLeft: Radius.circular(7.0),
                                  ),
                                ),
                                child: AutoSizeText(
                                  "Ocorrencia",
                                  maxLines: 1,
                                  maxFontSize: 17.0,
                                  minFontSize: 15.0,
                                  style: TextStyle(
                                      color: fontColor,
                                      fontSize: 17.0,
                                      fontFamily: 'MontHeavy'),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 38.0),
                            child: Container(
                              width: (160.0),
                              height: 60.0,
                              child: RaisedButton(
                                elevation: 0.0,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      new MyCustomRoute(
                                          builder: (context) =>
                                              ClassesPage('comunicado')));
                                },
                                highlightColor: Colors.white70,
                                color: Color(hexColor("#c7ffda")),
                                shape: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 0.5),
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(7.0),
                                    bottomRight: Radius.circular(7.0),
                                  ),
                                ),
                                child: AutoSizeText(
                                  "Comunicado",
                                  maxLines: 1,
                                  maxFontSize: 17.0,
                                  minFontSize: 15.0,
                                  style: TextStyle(
                                      color: fontColor,
                                      fontSize: 17.0,
                                      fontFamily: 'MontHeavy'),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
