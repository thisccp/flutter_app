import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_app/ui/studentsPage.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter_app/ui/descriptionPage.dart';

class DatePage extends StatefulWidget {

  var origin;
  DatePage(this.origin);

  @override
  _DatePageState createState() => _DatePageState(this.origin);
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
MediaQueryData sizeScreen;

String dateS = sistemDate.toString();
String monthS = sistemMonth.toString();
String yearS = sistemYear.toString();
int sistemYear = int.parse(formatDate(DateTime.now(),[yyyy]));
int sistemMonth = int.parse(formatDate(DateTime.now(),[mm]));
int sistemDate = int.parse(formatDate(DateTime.now(),[dd]));

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}

class _DatePageState extends State<DatePage> {

  var origin;
  _DatePageState(this.origin);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: fontColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, left: 30.0),
                  child: AutoSizeText(
                    "Data",
                    maxLines: 1,
                    maxFontSize: 50.0,
                    minFontSize: 25.0,
                    style: TextStyle(fontFamily: 'montHeavy', fontSize: 50.0),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      DatePicker.showDatePicker(
                        context,
                        showTitleActions: true,
                        locale: 'i18n',
                        minYear: 2019,
                        maxYear: 2030,
                        initialYear: sistemYear,
                        initialMonth: sistemMonth,
                        initialDate: sistemDate,
                        dateFormat: 'dd-mm-yyyy',
                        onChanged: (date, month, year) { },
                        onConfirm: (year, month, date) {
                          setState(() {
                            dateS = date.toString();
                            monthS = month.toString();
                            yearS = year.toString();
                          });

                        },
                      );
                    },
                    child: AutoSizeText(
                      "$dateS/$monthS/$yearS",
                      maxLines: 1,
                      maxFontSize: 50.0,
                      minFontSize: 40.0,
                      style:
                      TextStyle(fontFamily: 'montExtraLight', fontSize: 50.0),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if(this.origin == 'ocorrencia'){
            Navigator.push(
              context, new MyCustomRoute(builder: (context) => StudentsPage()));
          } else {
            Navigator.push(
              context, new MyCustomRoute(builder: (context) => DescriptionPage()));
          }
        },
        child: Icon(Icons.navigate_next),
        backgroundColor: fontColor,
      ),
    );
  }
}