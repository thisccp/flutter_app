import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app/ui/eventPage.dart';
import 'package:flutter_app/ui/historicPage.dart';

class TabsPage extends StatefulWidget {
  @override
  _TabsPageState createState() => _TabsPageState();
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);

class _TabsPageState extends State<TabsPage> {
  int _selectedIndex = 0;
  final _widgetOptions = [
    EventPage(),
    HistoricPage(),
  ];

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text("Você quer sair?"), actions: <Widget>[
              FlatButton(
                child: Text("Sim"),
                onPressed: () => Navigator.pop(context, true),
              ),
              FlatButton(
                child: Text("Não"),
                onPressed: () => Navigator.pop(context, false),
              ),
            ]));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Image.asset(
                  "assets/icons8-calendar-plus-50.png",
                  width: 30.0,
                  color: fontColor,
                  height: 30.0,
                ),
                title: Text(
                  'Eventos',
                  style: TextStyle(),
                )),
            BottomNavigationBarItem(
                icon: Image.asset(
                  "assets/icons8-report-card-50.png",
                  width: 30.0,
                  color: fontColor,
                  height: 30.0,
                ),
                title: Text(
                  'Historico',
                  style: TextStyle(),
                )),
          ],
          currentIndex: _selectedIndex,
          fixedColor: fontColor,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
