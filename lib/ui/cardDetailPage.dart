import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:clipboard_manager/clipboard_manager.dart';

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
Color cardLineColor = Color(hexColor("#c7ffda"));

hexColor(String colorhexcode) {
  String colornew = '0xff' + colorhexcode;
  colornew = colornew.replaceAll('#', '');
  int colorint = int.parse(colornew);
  return colorint;
}


// Variável que armazena o tamanho de alguma midia ou tela
MediaQueryData sizeScreen;

final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

class CardDetailPage extends StatelessWidget {
  var eventDetail;

  CardDetailPage(this.eventDetail);

  Row buildClasses(item) {
    if (item.containsKey('classes')) {
      return Row(
        children: <Widget>[
          AutoSizeText(
            item['classes'],
            maxLines: 1,
            maxFontSize: 15.0,
            minFontSize: 10.0,
            style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w300),
          ),
        ],
      );
    } else {
      return Row();
    }
  }

  @override
  Widget build(BuildContext context) {
    sizeScreen = MediaQuery.of(context);

    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          iconTheme: IconThemeData(
            color: fontColor,
          ),
          backgroundColor: Colors.transparent,
          title: AutoSizeText(
            "Detalhes",
            maxFontSize: 30.0,
            minFontSize: 25.0,
            style: TextStyle(
                fontFamily: "MontHeavy", fontSize: 30.0, color: fontColor),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
            child: Container(
              color: Colors.white,
              height: sizeScreen.size.height - 140.0,
              child: Row(
                children: <Widget>[
                  Container(
                    height: sizeScreen.size.height - 140.0,
                    width: 10.0,
                    margin: EdgeInsets.only(right: 10.0),
                    decoration: BoxDecoration(
                        color: cardLineColor,
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(3.0),
                          bottomLeft: const Radius.circular(3.0),
                        )),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(top: 25.0, left: 10.0),
                      child: Column(children: <Widget>[
                        Row(
                          children: <Widget>[
                            AutoSizeText(
                              this.eventDetail['name'],
                              maxLines: 1,
                              maxFontSize: 20.0,
                              minFontSize: 15.0,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                          child: Row(
                            children: <Widget>[
                              AutoSizeText(
                                this.eventDetail['date'],
                                maxLines: 1,
                                maxFontSize: 15.0,
                                minFontSize: 10.0,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding:
                                const EdgeInsets.only(left: 20.0, top: 10.0),
                            child: buildClasses(this.eventDetail)),
                        Padding(
                          padding: EdgeInsets.only(left: 20.0),
                          child: Row(
                            children: <Widget>[
                              AutoSizeText(
                                "",
                                maxLines: 1,
                                maxFontSize: 15.0,
                                minFontSize: 10.0,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0, right: 20.0),
                          child: AutoSizeText(
                            "Descrição",
                            maxFontSize: 25.0,
                            minFontSize: 20.0,
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(right: 15.0),
                            child: GestureDetector(
                              onLongPress: () {
                                ClipboardManager.copyToClipBoard(
                                        this.eventDetail['data'])
                                    .then((result) {
                                  final snackBar = SnackBar(
                                    content: Text('Copiado'),
                                    action: SnackBarAction(
                                      label: '',
                                      onPressed: () {},
                                    ),
                                  );
                                  scaffoldKey.currentState
                                      .showSnackBar(snackBar);
                                });
                              },
                              child: AutoSizeText(
                                this.eventDetail['data'],
                                maxLines: 24,
                                textAlign: TextAlign.justify,
                                maxFontSize: 15.0,
                                minFontSize: 8.0,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        )
                      ]),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
