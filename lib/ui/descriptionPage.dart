import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_app/ui/eventPage.dart';
import 'package:flutter_app/ui/eventPage.dart';

class DescriptionPage extends StatefulWidget {
  @override
  _DescriptionPageState createState() => _DescriptionPageState();
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
MediaQueryData sizeScreen;

class _DescriptionPageState extends State<DescriptionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: fontColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 30.0, left: 30.0),
                child: AutoSizeText(
                  "Descricao",
                  maxLines: 1,
                  maxFontSize: 50.0,
                  minFontSize: 25.0,
                  style: TextStyle(fontFamily: 'montHeavy', fontSize: 50.0),
                ),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 60.0, left: 20.0, right: 20.0),
            child: TextField(
              maxLines: 4,
              autofocus: false,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 40.0),
              child: FlatButton(
                splashColor: fontColor,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                              title: Text("Enviado com sucesso"),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text("Ok"),
                                  onPressed: () {
                                    Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
                                  },
                                ),
                              ]));
                },
                child: AutoSizeText(
                  "Enviar",
                  maxFontSize: 30.0,
                  minFontSize: 15.0,
                  style: TextStyle(fontSize: 30.0),
                ),
              ))
        ]),
      ),
    );
  }
}
