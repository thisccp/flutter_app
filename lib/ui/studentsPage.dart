import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_app/ui/cardDetailPage.dart';
import 'package:flutter_app/ui/descriptionPage.dart';

class StudentsPage extends StatefulWidget {
  @override
  _StudentsPageState createState() => _StudentsPageState();
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
MediaQueryData sizeScreen;

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}

class _StudentsPageState extends State<StudentsPage> {

  List<Widget> generateStudents() {

    List<String> students = new List();
    students.add('Rafael Shidomi');
    students.add('Thiago Favaro');
    students.add('Alexando Gobbato');
    students.add('Juliana Silva');
    students.add('Ana Albuquerque');
    students.add('José Aldo');
    students.add('Cleiton Asta');
    students.add('Anderson Silva');
    students.add('Sabrina Nogueira');
    students.add('Caroline Santos');

    List<Widget> cards = new List();
    students.forEach((item){
      cards.add(new CustomCard(item));
    });

    return cards;
  }

  @override
  Widget build(BuildContext context) {
    sizeScreen = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: fontColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, left: 30.0),
                  child: AutoSizeText(
                    "Alunos",
                    maxLines: 1,
                    maxFontSize: 50.0,
                    minFontSize: 25.0,
                    style: TextStyle(fontFamily: 'montHeavy', fontSize: 50.0),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 2.0, left: 70.0),
                  child: AutoSizeText(
                    "Selecione os alunos",
                    maxLines: 1,
                    maxFontSize: 20.0,
                    minFontSize: 15.0,
                    style:
                        TextStyle(fontFamily: 'montExtraLight', fontSize: 20.0),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: new Column(
                children: generateStudents(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  var studentName;

  CustomCard(this.studentName);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(context,
              new MyCustomRoute(builder: (context) => DescriptionPage()));
        },
        child: Card(
          margin: EdgeInsets.only(left: 20.0, right: 20.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 0.0, right: 20.0),
                        child: Container(
                          height: 60.0,
                          width: 10.0,
                          margin: EdgeInsets.only(right: 10.0),
                          decoration: BoxDecoration(
                              color: Color(hexColor("#c7ffda")),
                              borderRadius: BorderRadius.only(
                                topLeft: const Radius.circular(3.0),
                                bottomLeft: const Radius.circular(3.0),
                              )),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    child: AutoSizeText(
                      this.studentName,
                      maxLines: 1,
                      maxFontSize: 20.0,
                      minFontSize: 15.0,
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 2.0, right: 2.0),
                    child: Container(
                      height: 60.0,
                      width: 1.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

hexColor(String colorhexcode) {
  String colornew = '0xff' + colorhexcode;
  colornew = colornew.replaceAll('#', '');
  int colorint = int.parse(colornew);
  return colorint;
}