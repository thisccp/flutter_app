import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_app/ui/datePage.dart';

class ClassesPage extends StatefulWidget {
  var origin;
  ClassesPage(this.origin);

  @override
  _ClassesPageState createState() => _ClassesPageState(this.origin);
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
MediaQueryData sizeScreen;

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}

class _ClassesPageState extends State<ClassesPage> {
  var origin;
  _ClassesPageState(this.origin);

  List<Widget> turmas = new List.generate(5, (i) => new TurmasRows());
  List<Widget> turmasTwo = new List.generate(5, (i) => new TurmasRows());

  @override
  Widget build(BuildContext context) {
    sizeScreen = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: fontColor,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, left: 30.0),
                  child: AutoSizeText(
                    "Turmas",
                    maxLines: 1,
                    maxFontSize: 50.0,
                    minFontSize: 25.0,
                    style: TextStyle(fontFamily: 'montHeavy', fontSize: 50.0),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 2.0, left: 70.0),
                  child: AutoSizeText(
                    "Selecione as turmas",
                    maxLines: 1,
                    maxFontSize: 20.0,
                    minFontSize: 15.0,
                    style:
                        TextStyle(fontFamily: 'montExtraLight', fontSize: 20.0),
                  ),
                )
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Column(
                            children: turmas,
                          ),
                          Column(
                            children: turmasTwo,
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              new MyCustomRoute(builder: (context) => DatePage(this.origin)));
        },
        child: Icon(Icons.navigate_next),
        backgroundColor: fontColor,
      ),
    );
  }
}

class TurmasRows extends StatefulWidget {
  @override
  _TurmasRowsState createState() => _TurmasRowsState();
}

hexColor(String colorhexcode) {
  String colornew = '0xff' + colorhexcode;
  colornew = colornew.replaceAll('#', '');
  int colorint = int.parse(colornew);
  return colorint;
}

class _TurmasRowsState extends State<TurmasRows> {
  var isSelected = false;
  var mycolor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(right: 15.0)),
        Padding(
          padding: EdgeInsets.only(bottom: 20.0),
          child: GestureDetector(
            onTap: toggleSelection,
            child: Container(
              width: 140.0,
              height: 140.0,
              child: Card(
                  color: mycolor,
                  elevation: 16.0,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Container(
                                width: 60.0,
                                height: 60.0,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(hexColor("#c7ffda"))),
                                child: Center(
                                    child: Text(
                                  "3ºA",
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      color: Color(hexColor("#878684"))),
                                ))),
                          )
                        ],
                      ),
                    ],
                  )),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(left: 15.0, right: 15.0))
      ],
    );
  }

  void toggleSelection() {
    setState(() {
      if (isSelected) {
        mycolor = Colors.white;
        isSelected = false;
      } else {
        mycolor = Colors.grey[200];
        isSelected = true;
      }
    });
  }
}
