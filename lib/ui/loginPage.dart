import 'package:flutter/material.dart';
import 'package:flutter_app/ui/tabsPage.dart';
import 'package:transparent_image/transparent_image.dart';

Color barColor = const Color.fromARGB(255, 44, 51, 60);

String label1 = "";
String label2 = "";

final _emailController = TextEditingController();
final _passwordController = TextEditingController();

void loginButton () {

  // Controles para pegar texto ou alterações dos textfields
  print(_emailController);
  print(_passwordController);

  label1 = _emailController.text;
  label2 = _passwordController.text;

}

void callHome(BuildContext context){
  if(label1 == "" && label2 == "") {
    label1 = "Thiago";
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => TabsPage())
    );
  } else if (label1 != "adm@adm.com.br" || label2 != "admin"){
    label1 = "Usuário ou senha incorreto!";
  }
}

// Variável que armazena o tamanho de alguma midia ou tela
MediaQueryData tamanho;

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    tamanho = MediaQuery.of(context); //armazena o contexto na varíavel tamanho
    return Scaffold(
        body: SingleChildScrollView( // SingleChildScrollView deixa a tela scrollable
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(color: Colors.white),
                height: tamanho.size.height / 2.3,
                child: Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: ("http://www.csasp.g12.br/imagens/topo2.png"))
                ),
              ),
              Container(
                decoration: BoxDecoration(color: barColor),
                height: tamanho.size.height  / 1.769,
                width: tamanho.size.width,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 65.0),
                      child: Container(
                        width: tamanho.size.width - 55.0,
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              prefixIcon: Padding(
                                  padding: EdgeInsets.only(bottom: 7.0),
                                  child: Icon(
                                    Icons.mail_outline,
                                    color: Colors.grey,
                                    size: 20.0,)),
                              hintText: "Login",
                              hintStyle: TextStyle(color: Colors.grey),

                            ),
                          ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Container(
                        width: tamanho.size.width - 55.0,
                          child: TextFormField(
                            obscureText: true,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              prefixIcon: Padding(
                                  padding: EdgeInsets.only(bottom: 7.0),
                                  child: Icon(
                                    Icons.lock,
                                    color: Colors.grey,
                                    size: 20.0,)),
                              hintText: "Senha",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                          ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 70.0),
                      child: Container(
                        width: tamanho.size.width - 55.0,
                        height: 50.0,
                        child: OutlineButton(
                          onPressed: () {
                            callHome(context);
                          },
                          borderSide: BorderSide(color: Colors.grey),
                          splashColor: Colors.grey,
                          highlightColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(3.0),
                          ),
                          child: Text("Login", style: TextStyle(color: Colors.grey),),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
    );
  }
}



