import 'package:flutter/material.dart';
import 'package:flutter_app/ui/cardDetailPage.dart';
import 'package:auto_size_text/auto_size_text.dart';

class HistoricPage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

Color fontColor = Color(0xFF2C333C);
Color mainColor = Color(0xFF8CE6C6);
Color cardLineColor = Color(hexColor("#c7ffda"));
MediaQueryData sizeScreen;

hexColor(String colorhexcode) {
  String colornew = '0xff' + colorhexcode;
  colornew = colornew.replaceAll('#', '');
  int colorint = int.parse(colornew);
  return colorint;
}

class _HomePageState extends State<HistoricPage> {

  List<Widget> generateHistory() {

    var history = [
      {
        'name': 'Rafael Shidomi',
        'date': '21-01-2019',
        'data': 'O aluno colou na prova e se recusou a ir para a sala da coordenadora o inspetor de alunos Ricardo precisou acompanha-lo, o aluno ficará suspenso do campeonato interclasses, pois conforme regras já estabelecidas anteriormente apenas alunos sem ocorrências podem participar.'
      },
      {
        'name': 'Comunicado',
        'date': '20-01-2019',
        'data': 'teste teste teste',
        'classes': '2B, 3A, 3D'
      },
      {
        'name': 'Thiago Favaro',
        'date': '19-01-2019',
        'data': 'teste teste teste'
      },
      {
        'name': 'Comunicado',
        'date': '18-01-2019',
        'data': 'teste teste teste',
        'classes': '4A, 4B, 5A'
      },
      {
        'name': 'Comunicado',
        'date': '18-01-2019',
        'data': 'teste teste teste',
        'classes': '6A, 6B'
      },
      {
        'name': 'Ana Albuquerque',
        'date': '17-01-2019',
        'data': 'teste teste teste'
      },
      {
        'name': 'José Aldo',
        'date': '17-01-2019',
        'data': 'teste teste teste'
      }
    ];

    List<Widget> cards = new List();
    history.forEach((item){
      cards.add(new CustomCard(item));
    });

    return cards;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Center(
          child: AutoSizeText("Historico",
          maxFontSize: 30.0,
          minFontSize: 20.0,
          style: TextStyle(fontFamily: "MontHeavy", fontSize: 30.0, color: fontColor),
          ),
        ),
      ),
      body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: new Column(
              children: generateHistory(),
            ),
          )),
    );
  }
}

class CustomCard extends StatelessWidget {
  var history;

  CustomCard(this.history);
  
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(top: 15.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => CardDetailPage(this.history)));
        },
        child: Card(
          margin: EdgeInsets.only(left: 20.0, right: 20.0),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 0.0, right: 20.0),
                    child: Container(
                      height: 130.0,
                      width: 10.0,
                      margin: EdgeInsets.only(right: 10.0),
                      decoration: BoxDecoration(
                          color: cardLineColor,
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(3.0),
                            bottomLeft: const Radius.circular(3.0),
                          )
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[ Row(
                        children: <Widget>[
                          AutoSizeText(
                            this.history['name'],
                            maxLines: 1,
                            maxFontSize: 20.0,
                            minFontSize: 15.0,
                            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                          child: Row(
                            children: <Widget>[
                              AutoSizeText(
                                this.history['date'],
                                maxLines: 1,
                                maxFontSize: 15.0,
                                minFontSize: 10.0,
                                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                    ]),
                  ),
                  Expanded(child: Column(
                    children: <Widget>[
                      Image(image: AssetImage("assets/icons8-advance-30.png"))
                    ],
                  ))
                ],
              ),
            ],
          ),
        )
      ),
    );
  }
}
